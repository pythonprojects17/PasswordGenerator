import random
import sys


def generate(n, min_char, max_char):
    """
    function generate n characters between min_char and max_char.
    :password n: number of characters to generate
    :type n: int
    :param min_char: the min_char of the generator
    :type min_char: char
    :param max_char: the max_char of the generator
    :type max_char: char
    :return: a buffer of characters
    :rtype: str
    """
    temp = ''
    for _ in range(n):
        temp += chr(random.randint(ord(min_char), ord(max_char)))
    return temp


def generate_chars(n):
    """
    function generate characters except lower case, upper case and numbers

    :param n: n characters to generate
    :type n: int
    :return: a buffer of characters
    :rtype: str
    """
    valid_chars = [j for j in range(91, 97)]
    valid_chars += [k for k in range(58, 65)]
    valid_chars += [m for m in range(32, 48)]
    valid_chars += [t for t in range(123, 127)]
    random.shuffle(valid_chars)

    temp = ''
    for _ in range(n):
        temp += chr(random.choice(valid_chars))
    
    return temp


def get_length(length):
    """
    function split on length to four pieces

    :param length: number to split
    :type length: int
    :return: list of four numbers that equal to the length
    :rtype: list
    """
    l_1 = length // 2
    l_2 = length - l_1
    l_3 = l_1 // 2
    l_1 = l_1 - l_3
    l_4 = l_2 // 2
    l_2 = l_2 - l_4
    return [l_1, l_2, l_3, l_4]


def main():
    if len(sys.argv) != 2:
        sys.exit('Invalid number of arguments')

    password = ''
    length = int(sys.argv[1])
    split_length = get_length(length)
    random.shuffle(split_length)

    password += generate(split_length[0], 'A', 'Z')
    password += generate(split_length[1], 'a', 'z')
    password += generate(split_length[2], '0', '9')
    password += generate_chars(split_length[3])

    print(''.join(random.sample(password, len(password))))


if __name__ == '__main__':
    main()
